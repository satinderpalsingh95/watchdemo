//
//  InterfaceController.swift
//  WatchDemo WatchKit Extension
//
//  Created by MacStudent on 2019-10-15.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var jadoLabel: WKInterfaceLabel!
    
    var count:Int = 0;
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("-----Watch App Loaded");
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func watchButtonPressed() {
        print("Clicked the button");
        self.count = self.count + 1;
        self.jadoLabel.setText("\(count) Jadoooooo!!")
    }
}
